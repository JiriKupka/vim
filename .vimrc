syntax on
colorscheme monokai

set number

map <C-b> :w<CR>:!python %<CR>
set ts=4
set sw=4
